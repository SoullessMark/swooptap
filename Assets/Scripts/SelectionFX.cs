﻿using UnityEngine;
using System.Collections;

public class SelectionFX : MonoBehaviour {
	public string current;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter2D(Collider2D col){
		if(col.tag == "Selectable"){
			col.transform.localScale = new Vector3 (2f, 2f, 0);
			current = col.GetComponent<Selectable>().me;
	}
	}
	void OnTriggerExit2D(Collider2D col){
		if (col.tag == "Selectable") {
			col.transform.localScale = new Vector3 (1f, 1f, 0);
		}
	}
	void Selected(){
		PlayerPrefs.SetString ("Selected", current);
	}
}
