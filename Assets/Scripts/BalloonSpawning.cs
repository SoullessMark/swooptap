﻿using UnityEngine;
using System.Collections;

public class BalloonSpawning : MonoBehaviour {
	public GameObject Balloon;
	public float timeBetweenBallons = 10f;
	private float storedTime;
	// Use this for initialization
	void Start () {
		storedTime = timeBetweenBallons;
	}
	
	// Update is called once per frame
	void Update () {
		float second = Time.deltaTime * 60;
		timeBetweenBallons -= second;
		if (timeBetweenBallons <= 0) {
			Instantiate(Balloon,new Vector3(transform.position.x,Random.Range (-4.0f,2.0f),transform.position.z),transform.rotation);
			timeBetweenBallons = storedTime;

		}
	}
}
