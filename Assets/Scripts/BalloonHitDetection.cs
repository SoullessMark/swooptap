﻿using UnityEngine;
using System.Collections;

public class BalloonHitDetection : MonoBehaviour {
	public GameObject Poof;
	public float deathTimer = 5f;
	public float loadTime = 250f;
	public bool canLoad = false;
	public bool isDead = false;
	public AudioClip hit;
	public int score = 0;
	public int hiScore = 0;
	public Animator b;
	// Use this for initialization
	void Start () {
		hiScore = PlayerPrefs.GetInt ("Score");
		b = GameObject.FindWithTag ("Over").GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		canLoad = GameObject.FindWithTag ("Loader").GetComponent<MayLoad> ().canLoad;
		float second = Time.deltaTime * 60;
		if (isDead) {
			deathTimer -= second;
			if(deathTimer <= 0){
				Animator a = GetComponentInParent<Animator>();
				a.SetInteger("State",0);
				b.SetInteger("State",1);
				if(canLoad){
					loadTime -= second;
					if(loadTime <= 0){
						Application.LoadLevel("DaBaseGame");
					}
				}
			}
		}
		if (score > hiScore) {
			PlayerPrefs.SetInt("Score",score);
			PlayerPrefs.Save();
		}
	}
	void OnTriggerEnter2D(Collider2D col){
		if (isDead == false) {
			if (col.gameObject.tag == "Balloon") {
				AudioClip Pop = col.gameObject.GetComponent<BalloonBehaviour> ().Pop;
				AudioSource.PlayClipAtPoint (Pop, transform.position, .25f);
				for (int i = 0; i < 5; i++) {
					Instantiate (Poof, col.gameObject.transform.position, col.gameObject.transform.rotation);
				}
				score++;
				Destroy (col.gameObject);
			}
			if (col.gameObject.tag == "Death") {

				AudioSource.PlayClipAtPoint (hit, transform.position, .25f);
				Animator a = GetComponentInParent<Animator> ();
				a.SetInteger ("State", 1);
				PlayerPrefs.Save();
				isDead = true;
			}
		}
	}

}
