﻿using UnityEngine;
using System.Collections;

public class Availability : MonoBehaviour {
	public Selectable[] selectables;
	public Sprite Locked;
	// Use this for initialization
	void Start () {
		for (int i = 0; i < selectables.Length; i++) {
			int isUnlocked;
			isUnlocked = PlayerPrefs.GetInt(selectables[i].storedName);
			if(isUnlocked == 1){
				Sprite Default = selectables[i].reg;
				selectables[i].gameObject.GetComponent<SpriteRenderer>().sprite = Default;
				string Name = selectables[i].storedName;
				selectables[i].me = Name;
			}
			if(isUnlocked == 0){
				selectables[i].gameObject.GetComponent<SpriteRenderer>().sprite = Locked;
				selectables[i].me = null;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
