﻿using UnityEngine;
using System.Collections;

public class PoofSpawn : MonoBehaviour {
	public float poofSpeedX;
	public float poofSpeedY;
	public Sprite[] Poofs;
	public float dieTime = 2f;
	public float second;
	// Use this for initialization
	void Start () {
		poofSpeedX = Random.Range (-1.5f, 1.5f);
		poofSpeedY = Random.Range (-1.5f, 1.5f);
		int randoPoof = Random.Range (0, 5);
		SpriteRenderer me = GetComponent<SpriteRenderer> ();
		me.sprite = Poofs [randoPoof];
		second = Time.deltaTime * 60;
		dieTime = second * 225f;
	}
	
	// Update is called once per frame
	void Update () {
		second = Time.deltaTime * 60;
		dieTime -= second;

		transform.Translate (poofSpeedX * Time.deltaTime, poofSpeedY * Time.deltaTime, 0f);
		if(dieTime < 0){
			Destroy(this.gameObject);
		}
	}
}
