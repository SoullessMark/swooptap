﻿using UnityEngine;
using System.Collections;

public class SwitchLayer : MonoBehaviour {
	public AnimationClip CRANE_DROP;
	public Animator Crane;
	private bool HAS_FINISHED = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {


	if (Crane.GetInteger("State") == 1 && !HAS_FINISHED) {
			StartCoroutine(CONTINUED_FIXED_ANIM());
			HAS_FINISHED = true;
		}
	}
	IEnumerator CONTINUED_FIXED_ANIM(){
		yield return new WaitForSeconds(CRANE_DROP.length + .5f);
		GetComponent<SpriteRenderer>().sortingOrder = 6;
	}
}
