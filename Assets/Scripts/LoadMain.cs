﻿using UnityEngine;
using System.Collections;

public class LoadMain : MonoBehaviour {
	public AnimationClip ON_LEAVE_SCENE_LOAD;
	private bool LOAD_MAIN = false;
	private bool LOAD_CHAR_SELECT = false;
	private bool LOAD_CHAR_UNLOCK = false;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void Load(){
		LOAD_MAIN = true;
		StartCoroutine(LEAVE_SCENE_ANIM());
	}
	void LoadSelect(){
		LOAD_CHAR_SELECT = true;
		StartCoroutine(LEAVE_SCENE_ANIM());
	}
	void LoadUnlock(){
		LOAD_CHAR_UNLOCK = true;
		StartCoroutine(LEAVE_SCENE_ANIM());
	}
	IEnumerator LEAVE_SCENE_ANIM(){
		yield return new WaitForSeconds(ON_LEAVE_SCENE_LOAD.length + .1f);
		if(LOAD_MAIN){
			Application.LoadLevel("MainMenu");
		}else if(LOAD_CHAR_SELECT){
			Application.LoadLevel("SelectionScreen");
		}else if (LOAD_CHAR_UNLOCK){
			Application.LoadLevel("CharacterUnlock");
		}
	}

}
