﻿using UnityEngine;
using System.Collections;

public class EnemySpawning : MonoBehaviour {
	public GameObject enemyOG;
	public float timeBetweenEnemies = 10f;
	private float storedTime;
	// Use this for initialization
	void Start () {
		storedTime = timeBetweenEnemies;
	}
	
	// Update is called once per frame
	void Update () {
		float second = Time.deltaTime * 60;
		timeBetweenEnemies -= second;
		if (timeBetweenEnemies <= 0) {
			Instantiate (enemyOG,new Vector3(transform.position.x,Random.Range (-1.0f,4.5f),transform.position.z),transform.rotation);
			timeBetweenEnemies = storedTime;
		}
	}
}
